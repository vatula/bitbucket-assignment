# README #

The goal of a project is to improve the first experience for users in Bitbucket and get them to stick around. An attempt tries out an existing theory that people may like to explore repositories. A project gives new and existing users a way to explore Bitbucket repositories categorized by themes.

### Why? ###

An assumption being made that this might give new users an understanding of how people use Bitbucket. Showing what Bitbucket can offer to new users as a contribution platform also brings positive sentiments as it is offering «to see and be seen».

An Exploring functionality is also perceived as a strong «selling point» to OSS community and is mentioned multiple times on an articles that compare Bitbucket to it’s competitors.

### How do I get set up? ###

There is a fast way and a slow way to see project in action. Fast way is to run pre−built binaries. A Slow way is to first build it.

### The fast way ###

 * Get the [Binaries](https://bitbucket.org/vatula/bitbucket-assignment/downloads/build.zip)
 * Extract binaries to any folder
 * Start web server in the _build_ directory

The last point is to simply serve files statically.

 * If you have python3+ installed, you can run `python -m http.server 9990`
 * If you have python2+ installed, you can run `python -m SimpleHTTPServer 9990`

And then navigate to `http://localhost:9990/index.html` to open `index.html`.

### The slow way ###

 * Ensure you have `node.js` installed
 * Ensure you have `gulp` installed
 * Ensure you have `bower` installed
 * Ensure you have python 3+ installed
 * Ensure you have software to unzip and to unpack 7zip archives
 * Get sources
 * Get pre−generated [data](https://bitbucket.org/vatula/bitbucket-assignment/downloads/data.7z)
 * Extract `data.7z` to the `src` folder of a cloned project so that all csv and text files are on the same level with python scripts (data_format_json.py and the likes)
 * Open command prompt and navigate to the root folder of a project
 * Run `npm install`
 * Run `bower install`
 * Run `python data_format_json.py`
 * Run `gulp`

You should now have `build` directory with index.html file, app.js and a directory called data that has subdirectories and json files in it.

Now, start web server in the _build_ directory

### What will I see? ###

`index.html` is a scrapped landing page for an unregistered users with an `app.js` included. When you start web server and navigate to the index page, you should see a normal landing page with additional link on the top that reads _Explore_ next to _Features_ link

![1.jpg](https://bitbucket.org/repo/6erpGE/images/223141966-1.jpg)

Clicking on an _Explore_ link will open up categories view. Each category has a title and a number of repositories in it.

![2.jpg](https://bitbucket.org/repo/6erpGE/images/361959793-2.jpg)

Clicking on a category will open paginated repositories view which will have a link to the repository and a short description of a repository

![3.jpg](https://bitbucket.org/repo/6erpGE/images/3935540697-3.jpg)

### What am I actually looking at (technical)? ###

The data for the categories and repositories is a real data from Bitbucket. The process of generating that data is as follows:

* script `data_load.py` uses Bitbucket API to scrap all repositories into one file called data.txt. It is pretty heavy and by Jan 5, 2015 it was weighted about 800 Mb
 * script `data_format_csv.py` extracts repository names, urls and short descriptions and forms `data.csv` file
 * script `data_prepare_samples_csv.py` then takes `data.csv` and extracts 400 lines uniformly into a new `data_samples.csv`
 * User manually goes over `data_samples.csv` and assigns a label against each repository by replacing `?` sign in a first column with a token
 * Naive Bayes model is formed from a data_samples.csv. Naive Bayes classifiers proved to be relatively accurate in text classification problems. [TagHelper Tools](http://www.cs.cmu.edu/~cprose/TagHelper.html) were used to form a model, but any would do. A model in Weka format can be downloaded from [here](https://bitbucket.org/vatula/bitbucket-assignment/downloads/weka_bitbucket_label_classifier.zip)
 * Then model was used against `data.csv` to tag all repositories in the file automatically
 * script `data_format_json.py` forms paginated data in `json` format and assigns generated image pattern to each category, similarly to how GitHub generates images to it’s own categories (client side generation is unnecessary)

It should be pointed out though that only the last 3k lines from tagged repositories were used due to lousy, naive pager implementation. With lots of data pager would simply not fit into one line.

Learned model is not perfect as well and relatively often does mistakes, but can be easily improved given a day or two more to crunch some parameters.