let {Route, DefaultRoute, Link, RouteHandler, State} = ReactRouter;

let NavigationHandler = React.createClass({
    render: () => <Link to="explore">Explore</Link>
});

export default NavigationHandler;