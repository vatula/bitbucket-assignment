import {CategoryStyle} from '../styles/category';
import Category from '../components/category';

let ExploreHandler = React.createClass({
    componentDidMount: function() {
        let self = this;
        ajax.send({url: 'data/all.json', type: 'GET', dataType: 'json', success: result => {
            if (self.isMounted()) {
                self.setState(result);
            }
        }});
    },
    getInitialState: () => ({ }),
    render: function() {
        let state = this.state;
        return <ul style={CategoryStyle.container}> {
            Object.keys(state).map(category => <Category name={category} value={state[category]}/>)
        }</ul>
    }
});

export default ExploreHandler;