import {Strings} from '../strings';
import {Pager} from '../styles/pager';
import {Repo} from '../styles/repo';

let {Route, DefaultRoute, Link, RouteHandler, State, Navigation} = ReactRouter;

let CategoryHandler = React.createClass({
    mixins: [Navigation, State],
    setAssyncState: function(page=1) {
        let self = this;
        ajax.send({url: `data/${this.getParams().category}/${page}.json`, type: 'GET', dataType: 'json', success: result => {
            self.setState({entries: result.entries, page: page, total: result.total, image: result.image});
        }});
    },
    componentWillReceiveProps: function() {
        this.setAssyncState(this.getParams().page);
    },
    componentDidMount: function() {
        this.setAssyncState(this.getParams().page);
    },
    getInitialState: () => ({entries: [], page: 1, total: 1, image: ''}),
    pager: (current, active, category) => {
        let activeLinkStyle = {color: 'rgba(50, 50, 50, 0.3)'};
        return (current === active)
        ? <span style={activeLinkStyle}>{current}</span>
        : <Link style={Pager.repo.link} to="category" params={{category: category, page: current}}>{current}</Link>},

    render: function() {
        let state = this.state;
        let category = this.getParams().category;
        let key = `${category.toLowerCase()}_${state.page}_p`;
        let total = Array.apply(null, new Array(state.total)).map((_,i) => i);
        let pager = this.pager;
        let paginator = <ul style={Pager.repo.container}>{
            total.map(i => {
                let p = i+1;
                return <li style={Pager.repo.item} key={"pager_" + p}>{pager(p, Number(state.page), category)}
                </li>})
        }</ul>;
        let headerStyle = Object.assign({
            backgroundImage: `url('${state.image}')`
        }, Repo.title);

        return <div>
            <h3 style={headerStyle}>{Strings.categories[category]}</h3>
            {paginator}
            <ul style={Repo.container}>{
                state.entries.map((entry, i) => <li style={Repo.item} key={key + i}>
                    <a style={Repo.name} href={entry.url}>{entry.name}</a>
                    <span style={Repo.itemTitle}>{entry.title}</span>
                </li>)
            }</ul>
            {paginator}
        </div>
    }
});

export default CategoryHandler;