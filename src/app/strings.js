export var Strings = {
    categories: {
        'App': 'Applications',
        'Docs': 'Documentation and wiki',
        'Edu': 'Assignments and projects for the uni',
        'Game': 'Game dev and games',
        'Hardware': 'DIYers and hardware crunchers',
        'LibsAndFrameworks': 'Software Libraries and Frameworks',
        'PluginsAndExtensions': 'Plugins and Extensions',
        'Science': 'Science',
        'Tools': 'Tooling and scripting',
        'UserSpecific': 'Unlabeled'
    }
};