import {CategoryStyle} from '../styles/category';
import {Strings} from '../strings';

let {Route, DefaultRoute, Link, RouteHandler, State} = ReactRouter;

let Category = React.createClass({
    render: function() {
        let classes = `rexplore ${this.props.name.toLowerCase()}`;
        let style = Object.assign({backgroundImage: `url('${this.props.value.image}')`}, CategoryStyle.item);
        return <li class={classes} style={style}>
            <Link to="category" params={{category: this.props.name, page: 1}} style={CategoryStyle.link}>
                <span style={CategoryStyle.title}>{Strings.categories[this.props.name]}</span>
                <span style={CategoryStyle.length}>{this.props.value.length}</span>
            </Link>
        </li>
    }
});

export default Category;