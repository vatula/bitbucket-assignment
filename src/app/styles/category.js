export var CategoryStyle = {
    container: {
        display: 'block',
        width: '100%',
        padding: 0,
        margin: 0
    },
    item: {
        display: 'inline-block',
        height: 200,
        width: '25%',
        color: 'white',
        padding: 0,
        margin: '10px 10px'
    },
    link: {
        position: 'relative',
        display: 'block',
        width: '100%',
        height: '100%',
        color: 'inherit'
    },
    title: {
        display: 'inline-block',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%'
    },
    length: {
        display: 'inline-block',
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%'
    }
};