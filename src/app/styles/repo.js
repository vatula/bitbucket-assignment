export var Repo = {
    title: {
        position: 'relative',
        display: 'block',
        top: -30,
        lineHeight: '8rem',
        height: '8rem',
        verticalAlign: 'middle',
        color: '#fff',
        margin: 0
    },
    container: {
        display: 'block',
        WebkitColumnCount: '2',
        MozColumnCount: '2',
        columnCount: '2',
        margin: 0,
        padding: 0,
        textAlign: 'left'
    },
    item: {
        display: 'block',
        margin: '0 15px 15px 15px',
        padding: 15,
        backgroundColor: '#fff',
        borderRadius: 3,
        breakInside: 'avoid',
        WebkitColumnBreakInside: 'avoid',
        pageBreakInside: 'avoid',
        border: '1px solid rgba(50, 50, 50, 0.2)',
        boxShadow: '0px 5px 0px 0px rgba(50, 50, 50, 0.3)'
    },
    name: {
        display: 'inline-block'
    },
    itemTitle: {
        display: 'block',
        margin: '10px 0 0 0'
    }
};