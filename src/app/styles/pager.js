export var Pager = {
    repo: {
        container: {
            display: 'block',
            position: 'relative',
            margin: 0,
            padding: 10
        },
        item: {
            display: 'inline-block',
            margin: 0,
            padding: 0,
            width: '1.5rem',
            textAlign: 'middle'
        },
        link: {
            display: 'inline-block',
            width: '100%',
            height: '100%',
            verticalAlign: 'middle',
            color: '#000'
        }
    }
};