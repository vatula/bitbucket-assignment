import {Strings} from 'strings';
import {CategoryStyle} from 'styles/category';
import NavigationHandler from 'handlers/navigation';
import CategoryHandler from 'handlers/category';
import ExploreHandler from 'handlers/explore';

let {Route, DefaultRoute, RouteHandler, HistoryLocation} = ReactRouter;

let containerStyle = {
    position: 'relative',
    display: 'block',
    backgroundColor: '#f5f5f5',
    top: -65,
    paddingTop: 30
};
let AppHandler = React.createClass({ render: () => <RouteHandler/> });

let routes = (
    <Route name="app" path="/" handler={AppHandler}>
        <Route name="explore" handler={ExploreHandler} />
        <Route name="category" path="explore/:category/:page" handler={CategoryHandler} />
        <DefaultRoute handler={NavigationHandler}/>
    </Route>);

document.addEventListener(
    'DOMContentLoaded',
    () => ReactRouter.run(routes, (Handler, route) => {
            let node = document.getElementById('content');
            let component = <Handler/>;
            if (route.path === '/') {
                node = document.getElementById('explore');
                if (!node) {
                    node = document.createElement('li');
                    let container = document.querySelector('ul.aui-nav');
                    container.insertBefore(node, container.firstChild);
                }
            } else {
                component = <div style={containerStyle}>{component}</div>;
            }
            React.render(component, node);
        })
);