__author__ = 'vatula'
import os
import urllib.request
import re


def load_all(next_url):
    response = urllib.request.urlopen(next_url)
    raw = response.read().decode('utf-8')
    data = re.search('.*\"values\":\s*(.*)\s*\"page\".*', raw).group(1)

    with open('data.txt', 'a', encoding='utf-8') as text_file:
        text_file.write(data + '\n')

    next_url = re.search('.*\"next\":\s*\"(.*)\s*\"\s*.*', raw).group(1)
    print(next_url)
    return next_url

if not os.path.exists('data.txt'):
    open('data.txt', 'w', encoding='utf-8').close()

url = 'https://bitbucket.org/api/2.0/repositories'
while url:
    url = load_all(url)
