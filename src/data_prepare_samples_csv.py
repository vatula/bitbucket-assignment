__author__ = 'vatula'

import math
import os


def file_len(f_name):
    with open(f_name, 'r', encoding='utf-8') as f:
        lines = 0
        for l in f:
            data_field = l.split(',')[1]
            print(data_field)
            if data_field not in ['""', '"text"']:
                lines += 1
    return lines + 1

if os.path.exists('data_samples.csv'):
    os.remove('data_samples.csv')

if not os.path.exists('data_samples.csv'):
    with open('data_samples.csv', 'w', encoding='utf-8') as csv:
        csv.write('"label","text","repo_name","repo_url"\n')

each_nth = math.floor(file_len('data.csv')/400)

with open('data.csv', 'r', encoding='utf-8') as data:
    with open('data_samples.csv', 'a', encoding='utf-8') as csv:
        i = 0
        for data_line in data:
            if data_line.split(',')[1] not in ['""', '"text"']:
                i += 1
                if i % each_nth == 0:
                    csv.write(data_line)