__author__ = 'vatula'
import os
import re


def sanitize(txt):
    remap = {
        ord('"'): "'"
    }
    txt = txt.translate(remap)
    return txt[:-1] if txt.endswith('\\') else txt

if not os.path.exists('data.txt'):
    open('data.txt', 'w', encoding='utf-8').close()

if not os.path.exists('data.csv'):
    with open('data.csv', 'w', encoding='utf-8') as csv:
        csv.write('"label","text","repo_name","repo_url"\n')

with open('data.txt', 'r', encoding='utf-8') as data:
    with open('data.csv', 'a', encoding='utf-8') as csv:
        safe = lambda txt, index: txt[index] if len(txt) > index else ''
        for data_line in data:
            texts = re.findall('\"description\":\s*\"(.*?)\"', data_line) # text
            repo_urls = re.findall('\"clone\":\s*\[\{\"href\":\s*\"(.*?)\"', data_line) # repo_url
            repo_names = re.findall('\"fork_policy\":\s+\"\w+\",\s+\"name\":\s*\"(.*?)\"', data_line) # repo_name
            for i in range(len(texts)):
                data_field = ' '.join(re.findall(r'[\w\d]+', sanitize(safe(texts, i))))
                if data_field:
                    csv.write(u'{0},"{1}","{2}","{3}"\n'
                              .format('?', data_field, sanitize(safe(repo_names, i)), safe(repo_urls, i)))