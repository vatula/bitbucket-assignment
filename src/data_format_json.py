__author__ = 'vatula'
import json
import os
import shutil
import math
import re
from geopatterns import GeoPattern

page_num = 10
learned_data_file = 'bitbucket_SUPP_Output_label_data_small.csv'
source_data_file = 'data_small.csv'


def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [alist[i*length // wanted_parts: (i+1)*length // wanted_parts]
            for i in range(wanted_parts)]


def create_categories(data):
    filepath = 'data/all.json'
    if not os.path.exists(filepath):
        os.makedirs('data', exist_ok=True)
        file = open(filepath, 'w', encoding='utf-8')
        file.close()
    with open(filepath, 'w', encoding='utf-8') as categories_json:
        json.dump(data, categories_json)


def add_category_entry(category, entry, image, pages):
    category_dir = 'data/' + category
    shutil.rmtree(category_dir, ignore_errors=True)
    os.makedirs(category_dir, exist_ok=True)
    for index, part in enumerate(split_list(entry, pages)):
        filepath = category_dir + '/' + str(index + 1) + '.json'
        with open(filepath, 'w', encoding='utf-8') as category_json:
            json.dump({"image": image, "total": pages, "entries": part}, category_json)

shutil.rmtree('data', ignore_errors=True)

with open(learned_data_file, 'r', encoding='utf-8') as csv:
    with open(source_data_file, 'r', encoding='utf-8') as satelite:
        categories = {}
        category_entries = {}
        additional_columns = [["",""]]
        for j, line in enumerate(satelite):
            if not j:
                continue
            matched = re.search('[\w\?]+,\".*\",\"(.*)\",\"(.*)\"', line)
            additional_columns.append([matched.group(1), matched.group(2)])

        for i, line in enumerate(csv):
            if not i:
                continue
            more = additional_columns[i]
            columns = line.split(',')
            category = columns[1]
            p = float(columns[2])
            if category not in categories:
                pattern = GeoPattern(category.encode('ascii', 'ignore'), generator='xes')
                categories[category] = {"length": 0, "image": 'data:image/svg+xml;base64,' + pattern.base64_string}
            if p > 0.98:
                if category not in category_entries:
                    category_entries[category] = []
                category_entries[category].append({"P": p, "title": columns[3][1:-1], "url": more[1], "name": more[0]})
        for category in category_entries:
            category_length = len(category_entries[category])
            pages = math.ceil(category_length/page_num)
            categories[category]["length"] = category_length
            add_category_entry(category, category_entries[category], categories[category]["image"], pages)
        create_categories(categories)