var gulp = require('gulp'),
    template = require('gulp-template'),
    through = require('gulp-through'),
    path = require('path'),
    traceur = require('traceur'),
    react = require('react-tools').transform,
    amd = require('amd-optimize'),
    concat = require('gulp-concat'),
    del = require('del'),
    merge = require('merge-stream'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    extend = require('util')._extend;

var build = function() {
    return 'build';
};

var jsx = through('jsx', function(file) {
    file.contents = new Buffer(react(String(file.contents), {}));
});

var es6 = through('es6', function(file, config) {
    var filename = path.basename(file.path);
    var compiler = new traceur.NodeCompiler(config);
    var es5 = compiler.compile(String(file.contents), filename, filename);
    var sourceMap = new Buffer(compiler.getSourceMap()).toString('base64');
    file.contents = new Buffer(es5 + '//# sourceMappingURL=data:application/json;base64,' + sourceMap);

},{
    sourceMaps: true,
    arrayComprehension: true,
    symbols: true
});

gulp.task('compile:html', function() {
    return gulp.src('src/app/index.html').pipe(gulp.dest(build()))
});

gulp.task('compile:scripts', function() {
    var streamJsx = gulp.src('src/app/**/*.jsx').pipe(jsx());
    var streamJs = gulp.src(['src/app/**/*.js', '!src/app/vendor/**/*.js']);
    return merge(streamJsx, streamJs)
        .pipe(es6({modules: 'amd'}))
        .pipe(rename({ extname: '.js' }))
        .pipe(gulp.dest(build()));
});

gulp.task('bundle:scripts', ['compile:scripts'], function() {
    return gulp.src(build() + '/**/*.js')
        .pipe(amd('main', { baseUrl: build() }))
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest(build()));
});

gulp.task('copy:vendor', function() {
    var streamMinimized = gulp.src([
        'src/app/vendor/react/react.min.js',
        'src/app/vendor/react-router/dist/react-router.min.js',
        'src/app/vendor/amdlite/amdlite.min.js'
    ]);
    var streamUglified = gulp.src([
        'src/app/vendor/ajax/ajax.monkey.js',
        'node_modules/traceur/bin/traceur-runtime.js'
    ]).pipe(uglify());

    return merge(streamUglified, streamMinimized).pipe(concat('dependencies.js')).pipe(gulp.dest(build()));
});

gulp.task('copy:data', function() {
    return gulp.src(['src/data/**/*'], { read: true }).pipe(gulp.dest(build() + '/data'));
});

gulp.task('merge:scripts', ['bundle:scripts', 'copy:vendor'], function() {
    return gulp.src([build() + '/dependencies.js', build() + '/app.js']).pipe(concat('app.js')).pipe(gulp.dest(build()))
});

gulp.task('clean', ['merge:scripts', 'copy:data'], function(cb) {
    del([
        build() + '/*',
        '!' + build() + '/data{,/**}',
        '!' + build() + '/app.js',
        '!' + build() + '/index.html'
    ], function(err) { cb(err); });
});

gulp.task('default', ['compile:scripts', 'bundle:scripts', 'copy:vendor', 'copy:data', 'merge:scripts', 'compile:html', 'clean']);